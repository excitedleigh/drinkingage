# Worldwide Legal Drinking Age Machine Readable Dataset

When you're selling (or even just marketing) alcohol online, you need to check whether your visitors are legally allowed to drink in their country. This dataset lets you do that, modelling the legal drinking age for different types of alcohol, in different situations, for people of different religions (yes, that's a thing!) in different countries, and in different regions of those countries.

Designed for building websites that comply with [the Alcohol Beverages Advertising Code Scheme's Best Practice in Digital Media](http://www.abac.org.au/wp-content/uploads/2013/11/Best-Practice-in-Digital-and-Social-Media-November-2013.pdf) and international equivalents, which require age gating based on the LDA in the visitor's country. Data is sourced from the [International Alliance for Responsible Drinking](http://www.iard.org/policy-tables/minimum-legal-age-limits/).

**[Download the data here!](http://gitlab.com/abre/drinkingage/builds/artifacts/master/file/drinkingage.json?job=build)**

## Format

The structure is as follows in this example. The values for drinking ages may be integers, or they may be `null`, in which case drinking is totally illegal in this region.

```js
{
    "_generated": "1999-12-31T23:59:59+1030", // Datetime the file was generated, in ISO 8601 format.
    "_updated": "1999-12-31T23:59:59+1030", // Datetime the file data was last updated, in ISO 8601 format.
    "xx": { // ISO 3166-1 2-character country code.
        "rules": { // Rules for this country.
            "default": 21, // Fallback drinking age for this country. This key will always be present.
            "nonmuslim": 20, // Drinking age for non-Muslim drinkers.
                             // Ususally if set, 'default' will be null i.e. drinking is illegal for Muslims in this country.
            "alcvolXXY": 20, // Beverages with an alcohol content less than XX.Y%v/v
            "onprem": 20, // Beverages consumed on premises
            "offprem": 18, // Beverages consumed off premises
            "notspirits": 18, // All beverages except spirits
            "beer": 16, // Beer
            "wine": 16, // Wine
            "offprem-beer": 15, // Sometimes keys may be combined with a hyphen e.g. 'beer consumed off premises' in this example.
        },
        "regions": { // If any regions of this country have more permissive rules, this key will be present.
            "aa": { // ISO 3166-2 region code
                "rules": {"default": 18}, // Rules object with the same format as above
            }
        }
    }
}
```

## Build Process

The raw data is written in `drinkingage.hjson`, which is a [HJSON](http://hjson.org/) file; it is identical in structure to the output except that countries that only have a `default` value can be specified in a more terse format e.g. `au: 18`. It is converted to the output file by `build.py`.

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/abre/drinkingage">
    <span property="dct:title">Commercial Motor Vehicles Pty Ltd</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Worldwide Legal Drinking Age Machine Readable Dataset</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="AU" about="https://gitlab.com/abre/drinkingage">
  Australia</span>.
</p>