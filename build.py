from datetime import datetime
from pytz import timezone
import json
import sys
import argparse

import hjson
from pycountry import countries, subdivisions

def normalise_value(val):
    if isinstance(val, int) or val is None:
        return {'rules': {'default': val}}
    if 'regions' in val:
        return {
            'regions': {k: normalise_value(v) for k, v in val['regions'].items()},
            'rules': val['rules']
        }
    return {'rules': val}


def get_data():
    with open('drinkingage.hjson', 'r') as ifile:
        idata = hjson.load(ifile)

    odata = {k: normalise_value(v) if not k.startswith('_') else v for k, v in idata.items()}
    odata['_generated'] = datetime.now(timezone('Australia/Adelaide')).isoformat()
    return odata

def does_rule_match(rule, type, alcvol, prem):
    for subrule in rule.split('-'):
        if subrule == type:
            continue
        if subrule == 'onprem' and prem == 'on':
            continue
        if subrule == 'offprem' and prem == 'off':
            continue
        if alcvol and subrule.startswith('alcvol') and float(subrule[-3:]) / 10 > alcvol:
            continue
        return False
    return True

def get_age(rules, type, alcvol, prem):
    for rule, age in rules.items():
        if rule == 'default':
            continue
        if does_rule_match(rule, type, alcvol, prem):
            return age
    return rules['default']

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', nargs='?', type=argparse.FileType('w'),
                        default=sys.stdout)
    parser.add_argument('--type', nargs='?', choices=['beer', 'wine', 'nonspirits'])
    parser.add_argument('--alcvol', nargs='?', type=float)
    parser.add_argument('--prem', nargs='?', choices=['on', 'off'])
    parser.add_argument('--defaults', action='store_true')
    args = parser.parse_args()

    general_data = get_data()

    if args.defaults or args.type or args.alcvol or args.prem:
        specific_data = {}
        for country, data in general_data.items():
            if country.startswith('_'):
                specific_data[country] = data
                continue

            iso = countries.get(alpha_2=country.upper())
            newdata = {}
            if 'regions' in data:
                newdata['regions'] = {}
                for region, rdata in data['regions'].items():
                    isosub = subdivisions.get(code='{}-{}'.format(country, region).upper())
                    newdata['regions'][region] = {
                        'name': isosub.name if isosub else None,
                        'age': get_age(rdata['rules'], args.type, args.alcvol, args.prem),
                    }
            newdata['age'] = get_age(data['rules'], args.type, args.alcvol, args.prem)
            newdata['name'] = iso.name
            specific_data[country] = newdata
        specific_data['_args'] = sys.argv[1:]
    else:
        specific_data = general_data

    json.dump(specific_data, args.file)
